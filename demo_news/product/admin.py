from django.contrib import admin
from .models import Article, Autor

admin.site.register(Article)
admin.site.register(Autor)

# Register your models here.
