from django.db import models
from datetime import datetime

# Create your models here.

class Autor(models.Model):
    name = models.CharField(max_length=70)

    def __str__(self):
        return self.name


class Article(models.Model):
    name = models.CharField(max_length=70)
    autor = models.ForeignKey(Autor, on_delete= models.CASCADE,help_text='Este es el autor de el articulo')
    date = models.DateField(default=datetime.now(), help_text='Esta es la fecha de creacion de este  articulo')

    def __str__(self):
        return self.name


