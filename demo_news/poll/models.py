from django.db import models
import datetime


class Question(models.Model):
    question = models.CharField(max_length=75)
    pub_date = models.DateTimeField(default=datetime.datetime.now())

    def __str__(self):
        return self.question

    def was_published_recently(self):
        return self.pub_date > datetime.datetime.now() - datetime.timedelta(days=1)

class Chooce(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    chooce_text = models.CharField(max_length=20)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.chooce_text


# Create your models here.
