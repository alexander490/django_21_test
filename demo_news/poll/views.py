from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from .models import Question, Chooce
from django.urls import reverse
from django.views import generic
from django.template import loader
# Create your views here.

class IndexView(generic.ListView):
    template_name = 'poll/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')

class ResultView(generic.DetailView):
    model = Question
    template_name = 'poll/result.html'
    #context_object_name = 'question_details'

class DetailView(generic.DetailView):
    model = Question
    template_name = 'poll/details.html'
    #context_object_name = 'question_details'


def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')
    #template = loader.get_template('poll/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    #outpub = ','.join([q.question for q in res_question_list])
    #return HttpResponse(template.render(context, request))
    return render(request, 'poll/index.html', context)

def details(request, question):
    question_details = get_object_or_404(Question, pk=question)
    #template = loader.get_template('poll/index.html')
    context = {
        'question': question_details,
    }
    #outpub = ','.join([q.question for q in res_question_list])
    #return HttpResponse(template.render(context, request))
    return render(request, 'poll/details.html', context)

def result(request, question):
    obj_question = get_object_or_404(Question, pk=question)
    context = {'question':obj_question}
    return render(request, 'poll/result.html', context)
    #return HttpResponse('Estas mirando el resultado de tu pregunta %s.'%question)

def vote(request, question):
    if request.method == 'POST':
        post = request.POST
        question_details = get_object_or_404(Question, pk=question)
        try:
            obj_chooce = get_object_or_404(Chooce, pk=post['chooce'])
            obj_chooce.votes += 1
            obj_chooce.save()
        except:
            return render(request,'poll/details.html',{
                'question': question_details,
                'error_message': "You didn't select a choice."
            })
    return HttpResponseRedirect(reverse('poll:result_view', args=(question,)))
    #return HttpResponse('Estas mirando el voto de tu pregunta %s.'%question)